import React from 'react';
import { connect } from 'react-redux';
import { appLoad } from '../../actionCreators';
import { Route, Switch, Redirect } from 'react-router-dom';
import axios from 'axios';
import AppBar from './Bar'
import CssBaseline from '@material-ui/core/CssBaseline';
import CircularProgress from '@material-ui/core/CircularProgress';
import SignIn from '../SignIn';
import Home from '../Home';

const mapStateToProps = state => {
  return {
    hasLoadedApp: state.common.hasLoadedApp,
    isSignedIn: state.auth.isSignedIn
  }
};

const mapDispatchToProps = dispatch => ({
  onLoad: () => {
  	dispatch(appLoad())
  }
});

class App extends React.Component {
	componentWillMount() {
    const accessToken = window.localStorage.getItem('accessToken');
    if (accessToken) {
    	axios.defaults.headers.common['Authorization'] = accessToken;
    }

    this.props.onLoad();
  }

  render() {
  	const hasLoadedApp = this.props.hasLoadedApp;
  	const isSignedIn = this.props.isSignedIn;

  	if (!hasLoadedApp) {
  		return (
  			<CircularProgress/>
  		);
  	}

  	if (isSignedIn) {
  		return (
        <React.Fragment>
        	<CssBaseline/>
					<AppBar/>
	      	<Switch>
	      		<Route exact path="/">
	      			<Redirect to="/home" />
	      		</Route>
						<Route exact path="/home" component={Home} />
    			</Switch>
        </React.Fragment>
      );
  	}

  	return (
        <React.Fragment>
        	<CssBaseline/>
	      	<Switch>
      			<Route exact path="/" component={SignIn} />
    			</Switch>
				</React.Fragment>
  	);

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
