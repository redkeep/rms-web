import {
	SIGN_IN_API,
	APP_LOAD_API
} from './constants/actionTypes'
import { api } from './api'

export function appLoad() {
  return {
    type: APP_LOAD_API,
    request: api.user()
  }
}

export function signIn(userName, password) {
  return {
    type: SIGN_IN_API,
    request: api.signIn(userName, password)
  }
}