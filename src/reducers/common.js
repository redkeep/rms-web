import {
  APP_LOAD_API
} from '../constants/actionTypes';
import { mapToApiState } from './reducerUtils';

const defaultState = {
  hasLoadedApp: false
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD_API:
      const apiState = mapToApiState(action);
      return {
        ...state,
        user: apiState.responseData,
        hasLoadedApp: !!apiState.responseData || !!apiState.error
      }
    default:
      return state;
  }
};