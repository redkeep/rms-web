import {
  SIGN_IN_API,
  APP_LOAD_API
} from '../constants/actionTypes';
import { mapToApiState } from './reducerUtils';

const defaultState = {
  isSignedIn: false
};

export default (state = defaultState, action) => {
  let apiState;
  switch (action.type) {
  	case SIGN_IN_API:
      apiState = mapToApiState(action);
      return {
        ...state,
        isSigningIn: apiState.isInProgress,
        isSignedIn: !!apiState.responseData
      }
    case APP_LOAD_API:
      apiState = mapToApiState(action);
      return {
        ...state,
        isSignedIn: !!apiState.responseData
      }
    default:
      return state;
  }
};