export function mapToApiState(action) {
  const isInProgress = !!action.request;
  const error = action.error ? action.error : null;
  const responseData = action.response && action.response.data
  return { isInProgress, error, responseData };
}