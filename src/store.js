import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import { promiseMiddleware, localStorageMiddleware } from './middleware'
import logger from 'redux-logger'
import createRootReducer from './reducer'

const getMiddleware = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(routerMiddleware(history), promiseMiddleware, localStorageMiddleware);
  } else {
    return applyMiddleware(routerMiddleware(history), promiseMiddleware, localStorageMiddleware, logger);
  }
};

export const history = createBrowserHistory()

export const store = createStore(
  createRootReducer(history),
  compose(getMiddleware())
)