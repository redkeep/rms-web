export const apiSuffix = '_API';
export const API_START = 'API_START';
export const API_END = 'API_END';
export const SIGN_IN_API = 'SIGN_IN' + apiSuffix;
export const APP_LOAD_API = 'APP_LOAD' + apiSuffix;