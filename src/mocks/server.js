// server.js
const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('src/mocks/db.json');
const middlewares = jsonServer.defaults();

const accessToken = 'SOMERANDOMACCESSTOKEN1234';

server.use(middlewares);

server.use((req, res, next) => {
	if (req.url === '/login') {
		next();
		return;
	}

	if (req.header('Authorization') === accessToken) {
    next();
    return;
  }
   
  res.status(401).send("Unauthorized!");
});

server.post('/login', (req, res) => {
	const result = { accessToken }
  res.jsonp(result);
});

server.use(router);
server.listen(3000, () => {
  console.log('JSON Server is running')
});