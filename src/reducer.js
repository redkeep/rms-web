import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import common from './reducers/common';
import auth from './reducers/auth';

export default (history) => combineReducers({
	auth,
	common,
  router: connectRouter(history)
})