import axios from 'axios'

class API {
	constructor() {
		const baseUrl = 'http://localhost:3000';
		const options = {
    	baseURL: baseUrl
    };

	  this.client = axios.create(options);
	}

	user() {
		return () => this.client.get('/user')
	}

	signIn(userName, password) {
		return () => this.client.post('/login', {
			userName, 
			password
		})
	}
}

export const api = new API();