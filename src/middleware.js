import { 
  apiSuffix, 
  API_START, 
  API_END,
  SIGN_IN_API,
} from './constants/actionTypes'
import { mapToApiState } from './reducers/reducerUtils';

export const promiseMiddleware = store => next => action => {
  if (!action.type.endsWith(apiSuffix)) {
    next(action);
    return;
  }

  if (action.response || action.error) {
    next(action);
    return;
  }
  
  const request = action.request;
  if (!request) {
    return;
  }

  store.dispatch({ type: API_START, subType: action.type });

  request()
    .then(response => {
      store.dispatch({ type: action.type, response });
    })
    .catch(error => {
      store.dispatch({ type: action.type, error });
    })
    .finally(() => {
      store.dispatch({ type: API_END, subType: action.type });
    });

  next(action);
};

export const localStorageMiddleware = store => next => action => {
  if (action.type === SIGN_IN_API) {
    const apiState = mapToApiState(action);
    const accessToken = apiState.responseData && apiState.responseData.accessToken;
    if (!apiState.error && !apiState.isInProgress && accessToken) {
      window.localStorage.setItem('accessToken', accessToken);
    }
  }

  next(action);
};